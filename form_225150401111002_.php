<!DOCTYPE html>
<html>
<head>
    <title>Form PHP</title>
</head>
<body>
    <h2>Form Input</h2>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="nama">Nama:</label><br>
        <input type="text" id="nama" name="nama"><br><br>

        <label>Jenis Kelamin:</label><br>
        <input type="radio" id="laki-laki" name="jenis_kelamin" value="Laki-laki">
        <label for="laki-laki">Laki-laki</label><br>
        <input type="radio" id="perempuan" name="jenis_kelamin" value="Perempuan">
        <label for="perempuan">Perempuan</label><br><br>

        <label for="kota">Kota:</label>
        <select id="kota" name="kota">
            <option value="Jakarta">Jakarta</option>
            <option value="Bandung">Bandung</option>
            <option value="Surabaya">Surabaya</option>
            <option value="Yogyakarta">Yogyakarta</option>
        </select><br><br>

        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nama = $_POST['nama'];
        $jenis_kelamin = $_POST['jenis_kelamin'];
        $kota = $_POST['kota'];

        echo "<h2>Informasi yang Anda masukkan:</h2>";
        echo "Nama: $nama <br>";
        echo "Jenis Kelamin: $jenis_kelamin <br>";
        echo "Kota: $kota";
    }
    ?>
</body>
</html>
